package lecture6;

import java.util.ArrayList;

public class Book {
	//field variables
	ArrayList<String> authors;
	int numPages;
	String title;
	
	//Constructor
	public Book(String author, int pages, String title){
		this(toList(author),pages,title);
	}

	public Book(ArrayList<String> authors, int pages, String title){
		this.authors = new ArrayList<String>();
		this.authors = authors;
		numPages = pages;
		this.title = title;
	}

	//helper methods
	private static ArrayList<String> toList(String author){
		ArrayList<String> authors = new ArrayList<String>();
		authors.add(author);
		return authors;
	}
	
	//methods
	public String getFirstAuthor() {
		return authors.get(0);
	}
	
	public void tearOutPage() {
		this.numPages--;
	}
	
	@Override
	public String toString() {
		return "Title: "+this.title+" by "+this.authors.toString();
	}
	

}
